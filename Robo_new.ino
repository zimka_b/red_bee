#include "extSignals.h"
#define START 0 //wait for
#define SEARCH 1	//look for enemy
#define RIGHTST 2 //partial signals
#define LEFTST 3
#define BACKST 4
#define FIGHT 5//aaaargh!
#define LINE 6 //watch line
#define STOP 7

//------------------------------------
#define NOIZEA 0
#define T_FROM_LINE 1
#define T_ROTATE 1
#define T_FIGHT_TO_SEARCH 1

//------------------------------------


extSignals all_signals;
int curr_state;
int next_state;

int flag_change;
int time_change;

int step = 0;
int time = 0;
int data = 0;
int right_Power, left_Power;

void setup()
{
	periph_Initialization();
	
	Serial.begin(9600);
	
	//delay(5000);
	//set_Velocity(50, 50);
	//delay(1000);
	//set_Velocity(0, 0);
}

void loop()
{
	Serial.print("step # ");
	Serial.println(step);
	Serial.println("status:");
	
	Serial.print("Update line data");
	time = micros();
	update_Line_Information();
	time = micros() - time;
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Left line data: ");
	time = micros();
	data = is_Left_Under_Line();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Right line data: ");
	time = micros();
	data = is_Right_Under_Line();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Enemy in front: ");
	time = micros();
	data = is_Enemy_In_Front();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Enemy behind: ");
	time = micros();
	data = is_Enemy_Behind();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Enemy of the left: ");
	time = micros();
	data = is_Enemy_Of_The_Left();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Enemy of the right: ");
	time = micros();
	data = is_Enemy_Of_The_Right();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Left analog distance: ");
	time = micros();
	data = get_Analog_Left_Distance();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Right analog distance: ");
	time = micros();
	data = get_Analog_Right_Distance();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	Serial.print("Acceleration module: ");
	time = micros();
	data = get_Accel();
	time = micros() - time;
	Serial.print(data);
	Serial.print("; operation time: ");
	Serial.print(time);
	Serial.println("mcs");
	
	delay(1000);
	Serial.println();
	Serial.println();
	Serial.println();
	step++;
}
