/* 
* extSignals.cpp
*
* Created: 28.09.2015 11:12:03
* Author: zimka
*/


#include "extSignals.h"

// default constructor
extSignals::extSignals()
{
	this->Accel = 0;
	this->ALeft = 0;
	this->ARight = 0;
	this->DBack = 0;
	this->DForward = 0;
	this->DLeft = 0;
	this->DRight = 0;
	this->LnLeft = 0;
	this->LnRight = 0;
	this->Starter = 0;
} //extSignals

// default destructor
extSignals::~extSignals()
{
} //~extSignals
extSignals& extSignals::operator=( const extSignals &c )
{
	this->Accel = c.Accel;
	this->ALeft = c.ALeft;
	this->ARight = c.ARight;
	this->DBack = c.DBack;
	this->DForward = c.DForward;
	this->DLeft = c.DLeft;
	this->DRight = c.DRight;
	this->LnLeft = c.LnLeft;
	this->LnRight = c.LnRight;
	this->Starter = c.Starter;
	
}


extSignals::extSignals( const extSignals &c )
{
	this->Accel = c.Accel;
	this->ALeft = c.ALeft;
	this->ARight = c.ARight;
	this->DBack = c.DBack;
	this->DForward = c.DForward;
	this->DLeft = c.DLeft;
	this->DRight = c.DRight;
	this->LnLeft = c.LnLeft;
	this->LnRight = c.LnRight;
	this->Starter = c.Starter;
	
}
