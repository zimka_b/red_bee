/* 
* extSignals.h
*
* Created: 28.09.2015 11:12:03
* Author: zimka
*/


#ifndef __EXTSIGNALS_H__
#define __EXTSIGNALS_H__


class extSignals
{
//variables
public:
int DRight;
int DLeft;
int DForward;
int DBack;

int ARight;
int ALeft;

int Accel;

int LnLeft;
int LnRight;

int Starter;
protected:
private:

//functions
public:
	extSignals();
	~extSignals();
	extSignals( const extSignals &c );
	extSignals& operator=( const extSignals &c );
protected:
private:

}; //extSignals

#endif //__EXTSIGNALS_H__
