extSignals collectSignals(){
	extSignals temp;
	
	temp.DRight = is_Enemy_Of_The_Right();
	temp.DBack = is_Enemy_Behind();
	temp.DLeft = is_Enemy_Of_The_Left();
	temp.DForward = is_Enemy_In_Front();
	
	temp.ALeft = get_Analog_Left_Distance();
	temp.ARight = get_Analog_Right_Distance();
	
	update_Line_Information();
	temp.LnLeft = is_Left_Under_Line();
	temp.LnRight = is_Right_Under_Line();
	
	//temp.Accel = get_Accel();
	
	//temp.Starter = get_Starter_Status();
	
	return temp;
}

int buildState(extSignals Sn){
	if ((Sn.DLeft) && (Sn.DRight)){
		return STOP;
	}
	if (Sn.LnLeft){
		return LINE; }
	
	if (Sn.LnRight){
		return LINE; }
	
	if (Sn.DLeft){
		return LEFTST; }
	
	if (Sn.DRight){
		return RIGHTST;	}
	
	if (Sn.DBack){
		return BACKST; }
	
	if (Sn.ALeft > NOIZEA){
		return FIGHT; }
	
	if (Sn.ARight > NOIZEA){
		return FIGHT; }
	
	if (Sn.DForward){
		return FIGHT; }
	
	if (!Sn.Starter){
		return START;}
	
	return SEARCH;
}


int solveStCollision(int oldSt, int newSt, int T)
{
	//TO LINE, NOT FROM FIGHT
	if (newSt == LINE) {
		if (oldSt != FIGHT){
			return 1; //LINE
		}
		else{
			return 0;//FIGHT
		}
	}
	
	//TO FIGHT
	if (newSt == FIGHT){
		return 1;
	}
	
	//FROM LINE, NOT TO FIGHT
	if (oldSt == LINE){
		if (T <= T_FROM_LINE){
			return 0;//LINE
		}
		else{
			return 1;//next_state
		}
	}
	
	//TO SEARCH
	if (newSt == SEARCH){
		if ((oldSt == LEFTST)||(oldSt == RIGHTST)){
			if (T <= (int)(T_ROTATE*0.3)){
				return 0;
			}
			else{
				return 1;
			}
		}
		if (oldSt == BACKST){
			if (T <= (int)(T_ROTATE*0.6)){
				return 0;
			}
			else{
				return 1;
			}
		}
		if (oldSt == FIGHT){
			if (T <= T_FIGHT_TO_SEARCH){
				return 0;
			}
			else{
				return 1;
			}
		}
		return 1;//ANY OTHER  - OK
	}

}

void doState(int st){
	if (st==STOP){
		set_Velocity(0,0);
	}
	if (st==SEARCH){
		set_Velocity(10, -10);
		return;
	}
	if (st == FIGHT){
		set_Velocity(40, 40);		
		return;
	}
	if (st == LINE){
		set_Velocity(-10,-10);
	}
	
	
}