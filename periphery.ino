
//---------- motor driver ---------------------------
#define LEFT_PWM_PIN 3
#define RIGHT_PWM_PIN 5
#define LEFT_DIRECT_PIN 2
#define RIGHT_DIRECT_PIN 4

//---------- digital distance sensor -----------------
#define DIGITAL_RIGHT_DISTANCE_PIN 15
#define DIGITAL_LEFT_DISTANCE_PIN 14
#define DIGITAL_FORWARD_DISTANCE_PIN 10
#define DIGITAL_BACK_DISTANCE_PIN 16

//---------- analog distance sensor ------------------
#define ANALOG_RIGHT_DISTANCE_PIN 21
#define ANALOG_LEFT_DISTANCE_PIN 20
float voltage[] = {2.7, 2.34, 2.00, 1.78, 1.56, 1.4, 1.265, 1.06, 0.92, 0.8, 0.74, 0.66, 0.52, 0.42, 0.36, 0.32};
float distance[] = {4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 25.0, 30.0, 35.0, 40.0};
int len = 16;

//---------- acceleration sensor ---------------------
#define X_ACCELERATION_PIN 9
#define Y_ACCELERATION_PIN 8
int x;
int y;
int sqr_abs_accel;
int default_x_acceleration;
int default_y_acceleration;

//---------- line sensor -----------------------------
#define LEFT_LINE_PIN 0
#define RIGHT_LINE_PIN 1

#include "QTRSensors.h"

unsigned int result[2];
QTRSensorsRC Line_Sensor((unsigned char[]) {LEFT_LINE_PIN, RIGHT_LINE_PIN}, 2) ;

//---------- start sensor ----------------------------
//#define STARTER_PIN 2


void periph_Initialization(){
	pinMode(LEFT_DIRECT_PIN, OUTPUT);
	pinMode(RIGHT_DIRECT_PIN, OUTPUT);
	pinMode(LEFT_PWM_PIN, OUTPUT);
	pinMode(RIGHT_PWM_PIN, OUTPUT);
	
	pinMode(DIGITAL_BACK_DISTANCE_PIN, INPUT);
	pinMode(DIGITAL_FORWARD_DISTANCE_PIN, INPUT);
	pinMode(DIGITAL_RIGHT_DISTANCE_PIN, INPUT);
	pinMode(DIGITAL_LEFT_DISTANCE_PIN, INPUT);
	
	delay(400);
	default_x_acceleration = analogRead(X_ACCELERATION_PIN);
	default_y_acceleration = analogRead(Y_ACCELERATION_PIN);
	return;
}

//---------- motor driver ---------------------------
void set_Velocity(int right_percent, int left_percent)/* percent of PWM(speed), sign = direction of movement */
{
	right_percent *= -1; //local problem
	if (right_percent > 0)
	{
		digitalWrite(RIGHT_DIRECT_PIN, HIGH);
		analogWrite(RIGHT_PWM_PIN, (int) 2.55*right_percent);
	}
	else
	{
		digitalWrite(RIGHT_DIRECT_PIN, LOW);
		analogWrite(RIGHT_PWM_PIN, (int) (-2.55*right_percent));
	}
	
	if (left_percent > 0)
	{
		digitalWrite(LEFT_DIRECT_PIN, HIGH);
		analogWrite(LEFT_PWM_PIN, (int) 2.55*left_percent);
	}
	else
	{
		digitalWrite(LEFT_DIRECT_PIN, LOW);
		analogWrite(LEFT_PWM_PIN, (int) (-2.55*left_percent));
	}
	return;
}


//---------- digital distance sensor -----------------
int is_Enemy_Behind(void) //distance for enemy < 30 cm
{
	if (digitalRead(DIGITAL_BACK_DISTANCE_PIN) == HIGH)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int is_Enemy_Of_The_Right(void) //distance for enemy < 30 cm
{
	if (digitalRead(DIGITAL_RIGHT_DISTANCE_PIN) != HIGH)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int is_Enemy_Of_The_Left(void) //distance for enemy < 30 cm
{
	if (digitalRead(DIGITAL_LEFT_DISTANCE_PIN) != HIGH)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int is_Enemy_In_Front(void) //distance for enemy < 30 cm
{
	if (digitalRead(DIGITAL_FORWARD_DISTANCE_PIN) != HIGH)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


//---------- analog distance sensor ------------------
//return distance in cm between 4 and 30, else "-1"
int get_Analog_Right_Distance(void)
{
	int out = analogRead(ANALOG_RIGHT_DISTANCE_PIN);
	int pos = 0;
	float volt_read = out * 0.004887;  //  5 / 1023 = 0.00488758553
	if((voltage[0] > volt_read) && (voltage[len-1] < volt_read))
	{
		// ���������, ��� ��������� ����������� ������� �������
		int i;
		for (i = 0; i < len-1; i++)
		{
			if(volt_read >= voltage[i+1])
			{
				pos = i;
				break;
			}
		}
	}
	else
	{
		return -1;
	}
	
	float y1 = voltage[pos];
	float x1 = distance[pos];
	float y2 = voltage[pos + 1];
	float x2 = distance[pos + 1];
	float distance_out = (x2 - x1)*(y1 - volt_read)/(y1 - y2) + x1;  // ��������� �� ������� ������������� 123 � 145
	
	return (int) distance_out;
}

//return distance in cm between 4 and 30, else "-1"
int get_Analog_Left_Distance(void)
{
	int out = analogRead(ANALOG_LEFT_DISTANCE_PIN);
	int pos = 0;
	float volt_read = out * 0.004887;  //  5 / 1023 = 0.00488758553
	if((voltage[0] > volt_read) && (voltage[len-1] < volt_read))
	{
		// ���������, ��� ��������� ����������� ������� �������
		int i;
		for (i = 0; i < len-1; i++)
		{
			if(volt_read >= voltage[i+1])
			{
				pos = i;
				break;
			}
		}
	}
	else
	{
		return -1;
	}
	
	float y1 = voltage[pos];
	float x1 = distance[pos];
	float y2 = voltage[pos + 1];
	float x2 = distance[pos + 1];
	float distance_out = (x2 - x1)*(y1 - volt_read)/(y1 - y2) + x1;  // ��������� �� ������� ������������� 123 � 145
	
	return (int) distance_out;
}


//---------- acceleration sensor ---------------------
int get_Accel(void)
{
	x = analogRead(X_ACCELERATION_PIN) - default_x_acceleration;
	y = analogRead(Y_ACCELERATION_PIN) - default_y_acceleration;
	return (int) (x*x + y*y);
}
int get_X_Accel(void)
{
	return analogRead(X_ACCELERATION_PIN) - default_x_acceleration;
}
int get_Y_Accel(void)
{
	return analogRead(Y_ACCELERATION_PIN) - default_y_acceleration;
}

void update_Line_Information (void)
{
	Line_Sensor.read(result);
}

int is_Left_Under_Line(void)
{
	if (result[1] < 150)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int is_Right_Under_Line(void)
{
	if (result[0] < 150)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//bool det_Starter_Status(void)
//{
//if (digitalRead(STARTER_PIN) == HIGH)
//{
//return 1;
//}
//else
//{
//return false;
//}
//}


